const express = require("express");
const morgan = require("morgan");
const app = express();
const router = express.Router();

var redis = require("redis"),
    client = redis.createClient();

client.on("error", function (err) {
    console.log("Error " + err);
});

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(morgan("dev"));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

app.use(router.post('/', (req, res, next) => {
  client.set(req.body.chave, req.body.valor, (err, response) => {
    if (err) {
      return res.status(500).send({
        error: err
      });
    } else {
      res.status(201).json({
        mensagem : response
      });
    }
  });
}));

app.use(router.get('/:chave', (req, res, next) => {
  client.get(req.params.chave, (err, response) => {
    if (err) {
      return res.status(500).send({
        error: err
      });
    } else {
      res.status(201).json({
        retorno : response
      });
    }
  });
}));

app.use(router.delete('/:chave', (req, res, next) => {
  client.del(req.params.chave, (err, response) => {
    if (err) {
      return res.status(500).send({
        error: err
      });
    } else {
      res.status(201).json({
        retorno : response
      });
    }
  });
}));

// Caso seja acessada uma rota que não existe
app.use((req, res, next) => {
  const error = new Error("Route Not found");
  error.status = 404;
  next(error);
});

// Resposta padrão caso haja algum erro
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: error.message
  });
});

module.exports = app;